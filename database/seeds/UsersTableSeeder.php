<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'     => 'Brian',
            'email'    => 'captbrogers@protonmail.ch',
            'avatar'   => 'default.jpg',
            'password' => bcrypt('password'),
        ]);
    }
}
