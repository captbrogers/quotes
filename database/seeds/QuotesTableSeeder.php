<?php

use Illuminate\Database\Seeder;

use App\Author;
use App\Quote;
use League\CommonMark\CommonMarkConverter;

class QuotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rawJsonString = file_get_contents(storage_path('app/quotes.json'));
        $quotes = json_decode($rawJsonString);
        $converter = new CommonMarkConverter();

        foreach ($quotes as $quote) {
            $author = Author::firstOrCreate([
                'name' => $quote->author
            ]);

            Quote::create([
                'uid'           => bin2hex(random_bytes(6)),
                'author_id'     => $author->id,
                'body'          => $converter->convertToHtml($quote->quote),
                'body_markdown' => $quote->quote,
                'year'          => $quote->year,
                'era'           => $quote->era,
            ]);
        }
    }
}
