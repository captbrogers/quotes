<?php

// global helpers go here

if (! function_exists('asset_url')) {
    function asset_url($uri)
    {
        if (substr($uri, 0, 1) == '/') {
            return env('APP_URL') . $uri;
        }
        return env('APP_URL') . '/' . $uri;
    }
}

if (! function_exists('generateUID')) {
    function generateUID()
    {
        $uuid = Ramsey\Uuid\Uuid::uuid4();
        $uuidArray = explode('-', $uuid);
        return end($uuidArray);
    }
}

if (! function_exists('generateUUID')) {
    function generateUUID()
    {
        return Ramsey\Uuid\Uuid::uuid4();
    }
}

if (! function_exists('activeLink')) {
    /**
     * Returns the string "active" if the current URI
     * matches one passed in. Useful for nav menus
     * where the link to the current page needs
     * to be different.
     *
     * @param string $uri A URI to compare against the current page
     * @return string Either 'active' or an empty string
     */
    function activeLink($uri)
    {
        if (request()->url() == route($uri)) {
            return 'active';
        }
        return '';
    }
}

if (! function_exists('timeAgo')) {
    function timeAgo($unixSeconds)
    {
        $now = Carbon\Carbon::now();
        $cachedAt = Carbon\Carbon::createFromTimestamp($unixSeconds);
        $diffInSeconds = $now->diffInSeconds($cachedAt);
        return $now->subSeconds($diffInSeconds)->diffForHumans();
    }
}

if (! function_exists('convertTime')) {
    /**
     * Take a float value for time and conver it to a
     * human-readable form.
     *
     * E.g. 3.5 becomes 3 hours and 30 minutes.
     *
     * @param float $decNumber The time in decimal notation
     * @return object Hour(s) and minute(s) as proerties
     */
    function convertTime($dec)
    {
        // start by converting to seconds
        $seconds = ($dec * 3600);

        // we're given hours, so let's get those the easy way
        $hours = floor($dec);

        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;

        // calculate minutes left
        $minutes = floor($seconds / 60);

        // remove those from seconds as well
        $seconds -= $minutes * 60;

        // return the time formatted HH:MM:SS
        return (object)[
            'hours'   => $hours,
            'minutes' => $minutes,
        ];
    }
}
