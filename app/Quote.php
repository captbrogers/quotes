<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Quote extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quotes';

    /**
     * The primary key for the model
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'int';

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uid';
    }

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'author_id',
        'body',
        'body_markdown',
        'year',
        'era',
    ];

    /**
     * The attributes that should be cast to native types
     * E.g. always cast a number as integer instead
     * of a string
     *
     * Your options are:
     * integer, real, float, double, string,
     * boolean, object, array, collection,
     * date, datetime, timestamp
     *
     * @var array
     */
    protected $casts = [
        'author_id' => 'integer',
        'year'      => 'integer',
    ];

    /**
     * The attributes that should be mutated to dates
     * E.g. deleted_at, published_at, etc
     *
     * @var array
     */
    protected $dates = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form
     *
     * @var array
     */
    protected $appends = [
        'quote_short',
        'uri',
    ];

    protected $with = [
        'author',
    ];

    /**
     * Boot function for Author model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->setRandomUid();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Private/Internal Methods
    |--------------------------------------------------------------------------
    |
    | The following methods are meant to be helpers specific
    | to this model.
    |
    */

    //

    /*
    |--------------------------------------------------------------------------
    | Accessors and Mutators
    |--------------------------------------------------------------------------
    |
    | These are methods used to alter existing properties before returning
    | them or to create pseudo-properties for the model.
    |
    */

    /**
     * Ensure that a random Universal ID is set for this model.
     *
     * @return void
     */
    public function setRandomUid()
    {
        if ($this->uid == null || ! property_exists($this, 'uuid')) {
            $this->uid = bin2hex(random_bytes(6));
        }
    }

    /**
     * Concatenate a year and it's era, otherwise return unknown.
     *
     * @return string
     */
    public function getYearAttribute(): string
    {
        if (is_null($this->attributes['year'])) {
            return 'unknown';
        }

        return $this->attributes['year'] . ' ' . $this->attributes['era'];
    }

    /**
     * Cut off the quote for use in table list of quote.
     *
     * @return string
     */
    public function getQuoteShortAttribute(): string
    {
        return str_limit($this->attributes['body_markdown'], 30);
    }

    /**
     * Concatenate the Author's keyname and Quote's UID to form a URI.
     *
     * @return string
     */
    public function getUriAttribute(): string
    {
        return 'quote/' . $this->author->keyname . '/' . $this->attributes['uid'];
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    |
    | These are methods that are an alias to more complicated operations
    | to a simple Eloquent method for the model.
    |
    */

    //

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    | Here you specify relations between this model and other models.
    |
    */

    /**
     * Establish a relationship with a single Author.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }
}
