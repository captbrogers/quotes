<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Quote;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $data = Cache::remember('quotes.stats', 60, function () {
            $numberOfAuthors = DB::table('authors')->select('uid')->count();
            $numberOfQuotes = DB::table('quotes')->select('uid')->count();

            return [
                'numberOfAuthors' => $numberOfAuthors,
                'numberOfQuotes'  => $numberOfQuotes,
                'cached_at'       => date('Y-m-d H:i:s'),
            ];
        });

        return view('dashboard.index')->with($data);
    }
}
