<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $data = [
            'user' => auth()->user(),
        ];

        return view('dashboard.settings')->with($data);
    }

    public function updateSettings(UserUpdateRequest $request)
    {
        $requestData = $request->only('name', 'email');
        $user = auth()->user();
        $user->update($requestData);
        return $user;
    }

    public function updateAvatar()
    {
        return request()->all();
    }
}
