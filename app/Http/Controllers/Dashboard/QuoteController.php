<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\AuthorRepository;
use App\Repositories\QuoteRepository;
use App\Author;
use App\Quote;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class QuoteController extends Controller
{
    public $authorRepo;

    public $quoteRepo;

    public function __construct(AuthorRepository $authorRepo, QuoteRepository $quoteRepo)
    {
        $this->authorRepo = $authorRepo;
        $this->quoteRepo = $quoteRepo;
    }

    public function index()
    {
        $data = [
            'quotes' => Quote::with('author')->paginate(),
        ];

        return view('dashboard.quotes.index')->with($data);
    }

    public function showCreateForm()
    {
        return view('dashboard.quotes.create');
    }

    public function store(Request $request)
    {
        $quote = $this->quoteRepo->create($request->only(['body_markdown', 'author_id', 'year']));
        Cache::forget('quotes.stats');
        return response()->json($quote);
    }

    public function showEditForm(Quote $quote)
    {
        $quote->load('author');

        $data = [
            'quote' => $quote,
            'authors' => Author::all(),
        ];

        return view('dashboard.quotes.edit')->with($data);
    }

    public function update(Quote $quote)
    {
        return $this->quoteRepo->update($quote, request()->only(['body_markdown', 'author_uid', 'year']));
    }

    public function delete(Quote $quote)
    {
        //
    }

    public function restore(Quote $quote)
    {
        //
    }
}
