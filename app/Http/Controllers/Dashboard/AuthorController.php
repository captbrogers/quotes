<?php

namespace App\Http\Controllers\Dashboard;

use App\Author;
use App\Http\Controllers\Controller;
use App\Repositories\AuthorRepository;

use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public $authorRepo;

    public function __construct(AuthorRepository $authorRepo)
    {
        $this->authorRepo = $authorRepo;
    }

    public function index()
    {
        $data = [
            'authors' => Author::withCount('quotes')->paginate(),
        ];

        return view('dashboard.authors.index')->with($data);
    }

    public function showCreateForm()
    {
        return view('dashboard.authors.create');
    }

    public function store(Request $request)
    {
        if ($author = Author::byName($request->input('name'))->first()) {
            return response()->json($author);
        }
        $author = $this->authorRepo->create(['name' => $request->input('name')]);
        return response()->json($author);
    }

    public function showEditForm(Author $author)
    {
        $data = [
            'author' => $author,
        ];
        return view('dashboard.authors.edit')->with($data);
    }

    public function update(Author $author)
    {
        //
    }

    public function delete(Author $author)
    {
        //
    }
}
