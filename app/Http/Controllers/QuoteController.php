<?php

namespace App\Http\Controllers;

use App\Author;
use App\Quote;

use Illuminate\Http\Request;

class QuoteController extends Controller
{
    public function random()
    {
        $quote = Quote::orderByRaw('RAND()')->take(1)->first();

        return redirect($quote->uri);
    }

    public function randomByAuthor(Author $author)
    {
        $data = [
            'quote' => Quote::where('author_id', $author->id)->orderByRaw('RAND()')->take(1)->first(),
        ];

        return view('quote')->with($data);
    }

    public function show(Author $author, Quote $quote)
    {
        $quote->load('author');
        $data = [
            'quote' => $quote,
        ];

        return view('quote')->with($data);
    }
}
