<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;
use Illuminate\Notifications\Messages\MailMessage;

class UserController extends Controller
{
    public function verifyUser()
    {
        $emailAddress = request()->input('email');
        $notifiable = User::where('email', $emailAddress)->firstOrFail();

        $mailMessage = new MailMessage;
        $mailMessage->subject(Lang::getFromJson('Verify Email Address'))
            ->line(Lang::getFromJson('Please click the button below to verify your email address.'))
            ->action(
                Lang::getFromJson('Verify Email Address'),
                $this->verificationUrl($notifiable)
            )
            ->line(Lang::getFromJson('If you did not create an account, no further action is required.'));

        return response()->json($mailMessage);
    }

    private function verificationUrl($user)
    {
        $time = Carbon::now()->addMinutes(60);
        $key = $user->getKey();
        return URL::temporarySignedRoute('verification.verify', $time, ['id' => $key]);
    }
}
