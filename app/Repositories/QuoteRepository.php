<?php

namespace App\Repositories;

use App\Author;
use App\Quote;

use League\CommonMark\Converter as CommonMarkConverter;
use League\CommonMark\DocParser as CommonMarkDocParser;
use League\CommonMark\Environment as CommonMarkEnvironment;
use League\CommonMark\HtmlRenderer as CommonMarkHtmlRenderer;

class QuoteRepository
{
    protected $createFields;

    protected $updateFields;

    public function __construct()
    {
        $newQuote = new Quote;
        $this->createFields = $newQuote->getFillable();
    }

    public function create(array $data)
    {
        $environment = CommonMarkEnvironment::createCommonMarkEnvironment();
        $converter = new CommonMarkConverter(new CommonMarkDocParser($environment), new CommonMarkHtmlRenderer($environment));

        $newQuote = Quote::firstOrCreate([
            'author_id'     => $data['author_id'],
            'body_markdown' => $data['body_markdown'],
            'body'          => $converter->convertToHtml($data['body_markdown']),
            'year'          => $data['year'],
        ]);
        return $newQuote;
    }

    public function update(Quote $quote, array $data)
    {
        $author = Author::find($data['author_id']);
        if (! $author) {
            return false;
        }

        $environment = CommonMarkEnvironment::createCommonMarkEnvironment();
        $converter = new CommonMarkConverter(new CommonMarkDocParser($environment), new CommonMarkHtmlRenderer($environment));

        $quote->body_markdown = $data['body_markdown'];
        $quote->body = $converter->convertToHtml($data['body_markdown']);
        $quote->author_id = $author->id;
        $quote->year = $data['year'];
        $quote->update();

        return $quote;
    }
}
