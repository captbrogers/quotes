<?php

namespace App\Repositories;

use App\Author;

class AuthorRepository
{
    public function create(array $data)
    {
        return Author::create([
            'name' => $data['name'],
        ]);
    }

    public function update(Author $author, array $data)
    {
        $author->name = $data['name'];
        $author->update();
        return $author;
    }
}
