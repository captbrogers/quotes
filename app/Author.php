<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Builder;

class Author extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'authors';

    /**
     * The primary key for the model
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'int';

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'keyname';
    }

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'name',
        'keyname',
    ];

    /**
     * The attributes that should be cast to native types
     * E.g. always cast a number as integer instead
     * of a string
     *
     * Your options are:
     * integer, real, float, double, string,
     * boolean, object, array, collection,
     * date, datetime, timestamp
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * The attributes that should be mutated to dates
     * E.g. deleted_at, published_at, etc
     *
     * @var array
     */
    protected $dates = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The accessors to append to the model's array form
     *
     * @var array
     */
    protected $appends = [
        //
    ];

    /**
     * Boot function for Author model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->setRandomUid();
            $model->setAuthorKeyname();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Private/Internal Methods
    |--------------------------------------------------------------------------
    |
    | The following methods are meant to be helpers specific
    | to this model.
    |
    */

    //

    /*
    |--------------------------------------------------------------------------
    | Accessors and Mutators
    |--------------------------------------------------------------------------
    |
    | These are methods used to alter existing properties before returning
    | them or to create pseudo-properties for the model.
    |
    */

    /**
     * Ensure that a random Universal ID is set for this model.
     *
     * @return void
     */
    public function setRandomUid()
    {
        if ($this->uid == null || ! property_exists($this, 'uuid')) {
            $this->uid = bin2hex(random_bytes(6));
        }
    }

    /**
     * Ensure the Author's name is transformed to a web-safe form.
     *
     * @return void
     */
    public function setAuthorKeyname()
    {
        if ($this->keyname == null || ! property_exists($this, 'keyname')) {
            $this->keyname = str_slug($this->name);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    |
    | These are methods that are an alias to more complicated operations
    | to a simple Eloquent method for the model.
    |
    */

    /**
     * Scope to allow quick searching by Author name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $name  Author's name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByName($query, $name): Builder
    {
        return $query->where('keyname', str_slug($name));
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    | Here you specify relations between this model and other models.
    |
    */

    public function quotes(): HasMany
    {
        return $this->hasMany(Quote::class);
    }
}
