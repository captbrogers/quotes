<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'namespace' => 'Auth'
], function () {
    // Login
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');

    // Logout
    Route::get('logout', 'LoginController@logout');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Request a password reset
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');

    // Reset a password
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

    Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
});

Route::group([
    'namespace'  => 'Dashboard',
    'prefix'     => 'dashboard',
    'middleware' => 'auth',
], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard.home');

    Route::get('/quote', 'QuoteController@index')->name('dashboard.quotes.index');
    Route::get('/quote/new', 'QuoteController@showCreateForm')->name('dashboard.quotes.createForm');
    Route::post('/quote/new', 'QuoteController@store')->name('dashboard.quotes.store');
    Route::get('/quote/{quote}', 'QuoteController@showEditForm')->name('dashboard.quotes.editForm');
    Route::post('/quote/{quote}', 'QuoteController@update')->name('dashboard.quotes.update');
    Route::delete('/quote/{quote}', 'QuoteController@delete')->name('dashboard.quotes.delete');
    //Route::restore('/quotes/{quote}', 'QuoteController@restore')->name('dashboard.quotes.restore');

    Route::get('/author', 'AuthorController@index')->name('dashboard.authors.index');
    Route::get('/author/new', 'AuthorController@showCreateForm')->name('dashboard.authors.createForm');
    Route::post('/author/new', 'AuthorController@store')->name('dashboard.authors.store');
    Route::get('/author/{quote}', 'AuthorController@showEditForm')->name('dashboard.authors.editForm');
    Route::post('/author/{quote}', 'AuthorController@update')->name('dashboard.authors.update');
    Route::delete('/author/{quote}', 'AuthorController@delete')->name('dashboard.authors.delete');
    //Route::restore('/author/{quote}', 'AuthorController@restore')->name('dashboard.authors.restore');

    Route::get('/settings', 'UserController@index')->name('dashboard.user.settings');
    Route::post('/settings', 'UserController@updateSettings')->name('dashboard.user.update');
    Route::post('/settings/avatar', 'UserController@updateAvatar')->name('dashboard.user.update_avatar');
});


Route::get('/random', 'QuoteController@random')->name('quotes.random');
Route::get('/quote/{author}', 'QuoteController@randomByAuthor')->name('quotes.author');
Route::get('/quote/{author}/{quote}', 'QuoteController@show')->name('quotes.show');
