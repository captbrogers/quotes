let mix = require('laravel-mix')
let tailwindcss = require('tailwindcss')
require('laravel-mix-purgecss')

mix.webpackConfig({
   resolve: {
       alias: {
           "@": path.resolve(
               __dirname,
               "resources/assets/js"
           )
       }
   }
})

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .options({
    processCssUrls: false,
    postCss: [
      tailwindcss('./tailwind-config.js'),
    ]
  })
  .purgeCss()
  .sass('resources/sass/dashboard.scss', 'public/css')
  .options({
    processCssUrls: false,
    postCss: [
      tailwindcss('./tailwind-config.js'),
    ]
  })
  .purgeCss()
  .sass('resources/sass/quote.scss', 'public/css')
  .options({
    processCssUrls: false,
    postCss: [
      tailwindcss('./tailwind-config.js'),
    ]
  })
  .purgeCss()
  .sass('resources/sass/font-families.scss', 'public/css')
  .purgeCss()

/*
if (mix.inProduction()) {
  mix.version()
}
 */
