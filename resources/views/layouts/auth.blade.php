@extends('layouts.base')


@section('meta')

@endsection


@section('css')
<link href="{{ asset_url('css/app.css') }}" rel="stylesheet">

@endsection


@section('top-js')

@endsection


@section('body')

    <div class="flex content-center h-full">
        <div class="form-wrapper flex w-full justify-center">
            <div class="flex self-center flex-col w-1/3 bg-white rounded-lg p-4">
                @yield('content')

            </div>
        </div>
    </div>

@endsection


@section('js')

@endsection
