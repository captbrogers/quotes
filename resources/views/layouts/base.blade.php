<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">

        <!-- author, title, description, etc (stuff for humans) -->
        <title>{{ $pageTitle ?? env('APP_NAME') }}</title>
        <meta name="description" content="{{ $pageDescription ?? '' }}">
        <meta name="author" content="{{ $pageAuthor ?? '' }}">
        <link rel="copyright" href="{{ $pageCopyright ?? '' }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- mobile stuff -->
        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <!-- helps with tablets or phones viewing the site -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--<meta http-equiv="Content-Security-Policy" content="default-src 'self'">-->

        <!-- Links to a JSON file that specifies "installation" credentials for the web applications -->
        <link rel="manifest" href="{{ asset_url('mix-manifest.json') }}">

        <!-- Links to information about the author(s) of the document -->
        <link rel="author" href="{{ asset_url('humans.txt') }}">

        <!-- Refers to a copyright statement that applies to the link's context -->
        <link rel="license" href="{{ asset_url('copyright.html') }}">

        <!-- favicons -->
        <!-- default, tried and true -->
        <link rel="shortcut icon" href="{{ asset_url('favicon.ico') }}">
        <!-- Apple Specific -->
        <link rel="apple-touch-icon-precomposed" href="{{ asset_url('apple-touch-icon-152x152.png') }}">
        <!-- MS browsers -->
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="{{ asset_url('mstile-144x144.png') }}">

        <!-- Android 5 Chrome Color -->
        <meta name="theme-color" content="{{ env('THEME_COLOR', '#2196f3') }}">

        <!-- Disable automatic detection and formatting of possible phone numbers -->
        <meta name="format-detection" content="telephone=no">

        <!-- Tells Google not to show the sitelinks search box -->
        <meta name="google" content="nositelinkssearchbox">

        <!-- Tells Google not to provide a translation for this document -->
        <meta name="google" content="notranslate">

        @yield('meta')

        <!-- CSS -->
        <link href="{{ asset_url('css/font-families.css') }}" rel="stylesheet">
        @yield('css')

        <!-- JS that must be executed before the document is loaded -->
        @yield('top-js')

    </head>
    <body>
        <div id="app">

            @yield('body')

        </div>

        <!-- JavaScript files -->
        @yield('js')

    </body>
</html>
