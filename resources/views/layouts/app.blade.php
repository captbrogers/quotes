@extends('layouts.base')


@section('meta')

@endsection


@section('css')
<link href="{{ asset_url('css/app.css') }}" rel="stylesheet">

@endsection


@section('top-js')

@endsection


@section('body')

    @yield('content')

@endsection


@section('js')

@endsection
