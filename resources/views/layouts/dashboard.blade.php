@extends('layouts.base')


@section('meta')

@endsection


@section('css')
<link href="{{ asset_url('css/dashboard.css') }}" rel="stylesheet">

@endsection


@section('top-js')

@endsection


@section('body')

    <nav class="flex items-center justify-between flex-wrap bg-white p-6">
        <div class="flex items-center flex-no-shrink text-black mr-6">
            <svg class="feather feather-feather" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true">
                <path d="M20.24 12.24a6 6 0 0 0-8.49-8.49L5 10.5V19h8.5z"></path>
                <line x1="16" y1="8" x2="2" y2="22"></line>
                <line x1="17" y1="15" x2="9" y2="15"></line>
            </svg>
            <span class="flex content-center font-semibold text-xl ml-4">Quotes</span>
        </div>
        <div class="block lg:hidden">
            <button class="flex items-center px-3 py-2 border rounded text-teal-lighter border-teal-light hover:text-white hover:border-white">
                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
            </button>
        </div>
        <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
            <div class="text-sm lg:flex-grow">
                <a href="{{ route('dashboard.home') }}" class="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-teal mr-4">Home</a>
                <a href="{{ route('dashboard.quotes.index') }}" class="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-teal mr-4">Quotes</a>
                <a href="{{ route('dashboard.authors.index') }}" class="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-teal">Authors</a>
            </div>
            <div class="flex">
                <a href="{{ route('dashboard.user.settings') }}" class="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-teal mr-4">{{ Auth::user()->name }}</a>
                <a href="{{ route('logout') }}" class="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-teal"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden"> @csrf </form>
            </div>
        </div>
    </nav>

    <div class="flex justify-center w-full my-4">
        <div class="flex flex-col w-3/4">

            <vue-snotify></vue-snotify>

@if (session('status'))
            <div class="flex w-full my-4 justify-between">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
@endif

            @yield('content')


        </div>
    </div>

@endsection


@section('js')
<script src="{{ asset_url('js/app.js') }}"></script>

@endsection
