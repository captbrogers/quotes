<div class="card rounded">
    <header class="card_header rounded bg-{{ $headerColor ?? 'grey-darkest' }} text-{{ $textColor ?? 'white' }}">
        <p class="text-xl">{{ $title ?? '' }}</p>
        <p>{{ $subtitle ?? '' }}</p>
    </header>
    <section class="card_body">
        {{ $body }}
    </section>
    @if (isset($footer))
    <footer class="card_footer">
        <small class="ml-2">{{ $footer }}</small>
    </footer>
    @endif
</div>
