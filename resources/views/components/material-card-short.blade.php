<div class="card-small rounded">
    <header class="card_header rounded bg-{{ $headerColor ?? 'grey-darkest' }} text-{{ $textColor ?? 'white' }}">
        @include("partials.svgs.$icon", ['width' => $iconSize])
    </header>
    <section class="card_body">
        <p class="font-sm text-grey-dark">{{ $title }}</p>
        <h2 class="mc-body-title">{{ $data }}</h2>
    </section>
    @if (isset($footer))
    <footer class="card_footer">
        <small class="ml-2">Last 24 Hours</small>
    </footer>
    @endif
</div>
