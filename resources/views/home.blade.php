@extends('layouts.app')


@section('meta')
@parent

@endsection


@section('css')
@parent

@endsection


@section('top-js')
@parent

@endsection


@section('content')
@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


@endsection


@section('js')
@parent

@endsection
