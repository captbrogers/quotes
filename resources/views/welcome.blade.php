<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css"> --}}

        <link href="{{ asset('css/quote.css') }}" rel="stylesheet">

        @include('partials.analytics')

    </head>
    <body id="app">
        <div class="flex-center h-full">

            <div class="content">
                <h1 class="text-5xl py-4">Quote Machine</h1>
                <p class="py-1">A small side-project to keep random quotes</p>
                <p class="py-1"><a href="{{ route('quotes.random') }}">Get a random quote</a></p>
            </div>
        </div>
    </body>
</html>
