<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link href="https://fonts.googleapis.com/css?family=Crimson+Text|Open+Sans:400,400i" rel="stylesheet">
        <link href="{{ asset('css/quote.css') }}" rel="stylesheet">

        @include('partials.analytics')

    </head>
    <body>
        <div class="flex-center h-full">
            <div class="content">
                <div class="quote mb-8">

                    {!! $quote->body !!}

                </div>
                <div class="author mb-4">
                    <p class="pb-2">- {{ $quote->author->name }}, {{ $quote->year }}</p>

                    <p class="text-sm py-2"><a href="{{ route('quotes.random') }}" title="Another completely random quote">Random</a> | <a href="{{ route('quotes.author', $quote->author->keyname) }}" title="Another random quote by this author">Random by this author</a></p>
                    <p class="text-sm py-2">Direct link: {{ route('quotes.show', [$quote->author->keyname, $quote->uid]) }}</p>
                </div>
            </div>
        </div>
    </body>
</html>
