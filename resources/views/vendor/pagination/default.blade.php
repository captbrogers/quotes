@if ($paginator->hasPages())
    <ul class="tailwind-pagination list-reset flex flex-row items-center">
        @if ($paginator->onFirstPage())
            <li class="flex mr-2 disabled">
                <span class="bg-grey-lighter border rounded p-2">&laquo;</span>
            </li>
            <li class="flex mr-2 disabled">
                <span class="bg-grey-lighter border rounded p-2">&lsaquo;</span>
            </li>
        @else
            <li class="flex mr-2">
                <a href="{{ $paginator->url(1) }}" rel="first" class="bg-teal-lighter text-black no-underline border rounded p-2">&laquo;</a>
            </li>
            <li class="flex mr-2">
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="bg-teal-lighter text-black no-underline border rounded p-2">&lsaquo;</a>
            </li>
        @endif

        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="flex mr-2 disabled">
                    <span class="bg-grey-lighter border rounded p-2">{{ $element }}</span>
                </li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="flex mr-2 active">
                            <span class="bg-teal-darker text-white no-underline border rounded p-2">{{ $page }}</span>
                        </li>
                    @else
                        <li class="flex mr-2">
                            <a href="{{ $url }}" class="bg-teal-lighter text-black no-underline border rounded p-2">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        @if ($paginator->hasMorePages())
            <li class="flex mr-2">
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="bg-teal-lighter text-black no-underline border rounded p-2">&rsaquo;</a>
            </li>
            <li class="flex mr-2">
                <a href="{{ $paginator->url($paginator->lastPage()) }}" rel="last" class="bg-teal-lighter text-black no-underline border rounded p-2">&raquo;</a>
            </li>
        @else
            <li class="flex mr-2 disabled">
                <span class="bg-grey-lighter border rounded p-2">&rsaquo;</span>
            </li>
            <li class="flex disabled">
                <span class="bg-grey-lighter border rounded p-2">&raquo;</span>
            </li>
        @endif
    </ul>
@endif
