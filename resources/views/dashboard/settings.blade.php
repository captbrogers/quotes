@extends('layouts.dashboard')


@section('meta')
@parent

@endsection


@section('css')
@parent

@endsection


@section('top-js')
@parent

@endsection


@section('content')

    <div class="flex flex-row justify-between">
        <div class="flex w-2/5">
            @component('components/material-card', ['title' => 'Your Profile', 'subtitle' => 'What it will look like', 'headerColor' => 'indigo-dark', 'textColor' => 'white'])
                @slot('body')
                    <avatar-form
                        :avatar-update-url="'{{ route('dashboard.user.update_avatar') }}'"
                        :avatar-image-url="'{{ url('images/avatars/'.$user->avatar) }}'"
                        :verify-email-url="'{{ route('api.user.verify') }}'"
                        :user="{{ $user }}"
                    ></avatar-form>
                @endslot
            @endcomponent
        </div>

        <div class="flex w-3/5">
            @component('components/material-card', ['title' => 'Edit Your Profile', 'subtitle' => 'Change it up, if you want', 'headerColor' => 'blue', 'textColor' => 'white'])
                @slot('body')
                    <div class="flex-col w-full p-4">
                        <profile-form
                            :name="'{{ $user->name }}'"
                            :email="'{{ $user->email }}'"
                            :update-url="'{{ route('dashboard.user.update') }}'">
                        </profile-form>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>

@endsection


@section('js')
@parent

@endsection
