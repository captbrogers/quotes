@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @component('components/material-card', ['headerColor' => 'blue', 'title' => "Quote: {$quote->uid}", 'subtitle' => 'Edit this quote'])

                @slot('body')
                    <form id="form_edit-quote" action="{{ route('dashboard.quotes.update', [$quote->uid]) }}" method="POST">
                        @csrf

                        <div class="row m-0">
                            <div class="flex w-1/2">
                                <select class="custom-select" name="author_uid">
                                @foreach($authors as $author)
                                    <option value="{{ $author->uid }}" {{ $author->id == $quote->author_id ? 'selected="selected"' : '' }}>{{ $author->name }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="col-md-3">
                                <input type="text" name="year" class="p-2" value="{{ $quote->year }}" placeholder="1979">
                            </div>

                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success btn-lg btn-block">Update</button>
                            </div>
                        </div>

                        <div class="row mt-4 mx-0">
                            <div class="col-md-6">
                                <div id="quote-preview">{!! $quote->body !!}</div>
                            </div>

                            <div class="col-md-6">
                                <textarea name="body_markdown" class="p-3">{{ $quote->body_markdown }}</textarea>
                            </div>
                        </div>
                    </form>
                @endslot
            @endcomponent
        </div>
    </div>
</div>
@endsection
