@extends('layouts.dashboard')


@section('meta')
@parent

@endsection


@section('css')
@parent

@endsection


@section('top-js')
@parent

@endsection


@section('content')

    <div class="flex w-full my-4 justify-between">
        <div class="flex w-1/3 mr-4">
            @include('components/material-card-short', ['icon' => 'message-square', 'iconSize' => '36', 'headerColor' => 'teal-dark', 'title' => 'Total Quotes', 'data' => $numberOfQuotes])
        </div>

        <div class="flex w-1/3 mx-4">
            @include('components/material-card-short', ['icon' => 'users', 'iconSize' => '36', 'headerColor' => 'indigo-dark', 'title' => 'Total Authors', 'data' => $numberOfAuthors])
        </div>

        <div class="flex w-1/3 ml-4">
            @include('components/material-card-short', ['icon' => 'award', 'iconSize' => '36', 'headerColor' => 'orange-dark', 'title' => 'Most Quoted', 'data' => 'Epictetus'])
        </div>
    </div>

    <div class="flex w-full my-4">
        @component('components/material-card', ['title' => 'Quick Quote', 'subtitle' => "Just fill it out to add a new quote", 'headerColor' => 'blue', 'textColor' => 'white'])
            @slot('body')

                    <quote-quick-add
                        :quote-post-url="'{{ route('dashboard.quotes.store') }}'"
                        :author-post-url="'{{ route('dashboard.authors.store') }}'"
                    >
                    </quote-quick-add>

                </form>
            @endslot
        @endcomponent
    </div>

@endsection


@section('js')
@parent

@endsection
