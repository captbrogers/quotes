@extends('layouts.dashboard')

@section('content')

    <div class="flex">
        @component('components/material-card', ['title' => 'All Authors', 'headerColor' => 'indigo-dark'])
            @slot('body')
            <!-- form -->
                <div class="flex flex-col w-full">
                    <div class="flex flex-row">
                        <div class="flex flex-row w-1/2">
                            {{ $authors->links() }}
                        </div>

                        <div class="flex justify-end w-1/2">
                            <button class="threeD-button threeD-button-red" disabled>
                                <i class="fa fa-trash-o mr-2"></i> Trash Selected
                            </button>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12">
                            <table class="table w-full mb-4">
                                <thead>
                                    <tr class="bg-grey-darkest text-white">
                                        <th class="p-3">&nbsp;</th>
                                        <th class="text-left pl-6 pr-3 py-3">Name</th>
                                        <th class="p-3">Quote Count</th>
                                        <th class="p-3">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($authors as $author)
                                    <tr>
                                        <td class="p-3">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="checkbox" id="" value="groupChecks" aria-label="...">
                                            </div>
                                        </td>
                                        <td>{{ $author->name }}</td>
                                        <td class="text-center">{{ $author->quotes_count }}</td>
                                        <td class="d-flex flex-row-reverse">
                                            <a href="{{ route('dashboard.authors.editForm', [$author->uid]) }}" class="btn btn-primary mr-2">
                                                @include('partials.svgs.edit', ['width' => '16'])
                                            </a>
                                            <button class="btn btn-danger">
                                                @include('partials.svgs.trash-2', ['width' => '16'])
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            {{ $authors->links() }}
                        </div>
                    </div>
                @endslot
            </div>
        @endcomponent
    </div>

@endsection
