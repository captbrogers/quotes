@extends('layouts.dashboard')

@section('content')

    <div class="flex">
        @component('components/material-card', ['title' => "Author {$author->uid}", 'headerColor' => 'indigo-dark'])
            @slot('body')
            <!-- form -->
                <div class="flex flex-col w-full">
                    <form id="form_edit-author" action="{{ route('dashboard.authors.update', [$author->uid]) }}" method="POST">
                        @csrf

                        <input type="text" id="name" name="name" value="{{ $author->name }}">
                    </form>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
