@extends('layouts.auth')

@section('content')

    <div class="auth-form-title flex flex-col bg-blue-dark text-white px-4 py-6 mb-4 rounded shadow-md-blue-dark text-xl items-center">
        <h1 class="text-center mb-4">Authentication</h1>
        <div class="flex flex-row w-1/3 justify-around"></div>
    </div>

    <form method="post" action="{{ route('login') }}" class="auth-form flex flex-col p-4">
        @csrf

        <div class="form-block">
            <div class="flex flex-col w-full{{ $errors->has('email') ? ' is-invalid' : '' }}">
                <label for="email" class="pb-2">{{ __('E-Mail Address') }}</label>
                <input type="email" name="email" id="email" class="px-3 py-3 w-full border-b border-b-grey-dark border-b-hover-color" placeholder="you@domain.com" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback mt-2" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-block mt-4">
            <div class="flex flex-col w-full{{ $errors->has('password') ? ' is-invalid' : '' }}">
                <label for="password" class="pb-2">{{ __('Password') }}</label>
                <input type="password" name="password" id="password" class="px-3 py-3 w-full border-b border-b-grey-dark border-b-hover-color" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback mt-2" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="flex items-center justify-between mt-4">
            <input type="checkbox" id="remember" name="remember" class="fancy-checkbox" {{ old('remember') ? 'checked' : '' }}>
            <label for="remember" class="fc-label">
                <span class="fc-span"></span>{{ __('Remember Me') }}<ins class="fc-ins"><i class="fc-icon">{{ __('Remember Me') }}</i></ins>
            </label>
            <a href="{{ route('password.request') }}" title="Request a new password" class="forgot-link">{{ __('I forgot my password') }}</a>
        </div>

        <div class="flex flex-row mt-6 items-center justify-center">
            <button type="submit" class="flex submit-button bg-blue-dark text-white justify-center font-bold w-1/3 py-3 rounded">{{ __('Login') }}</button>
        </div>
    </form>

@endsection
