
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
Vue.config.productionTip = false;

require('./waves');

import Snotify, { SnotifyPosition } from 'vue-snotify';
var snotifyOptions = {
    toast: {
        position: SnotifyPosition.centerTop
    }
};

Vue.use(Snotify, snotifyOptions);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('quote-quick-add', require('./components/QuoteQuickAdd.vue'));
Vue.component('material-card', require('./components/MaterialCard.vue'));

Vue.component('avatar-form', require('./components/User/AvatarForm.vue'));
Vue.component('profile-form', require('./components/User/ProfileForm.vue'));

const app = new Vue({
    el: '#app'
});
